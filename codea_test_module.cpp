/* ====================================================================
 * The C++ Object-oriented Development Environment for Apache (Codea)
 * Open Source Project License Version 1.0
 *
 * Copyright (c) 2001 The Codea Open Source Project.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Codea Open Source Project and is based upon software
 *        written by the Apache Software Foundation.  Codea is
 *        not a derivative work of Apache nor is Codea endorsed
 *        by the Apache Software Foundation."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Codea" and "Codea Open Source Project" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact the project administrator.
 *
 * 5. Products derived from this software may not be called "Codea",
 *    nor may "Codea" appear in their name, without prior written
 *    permission.
 *
 * 6. All conditions set forth in the Apache Software Foundation
 *    license must be met in addition to those set forth in this
 *    license agreement.  For details regarding the Apache Software
 *    Foundation license, please see http://www.apache.org/LICENSE.txt.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 */

/**
 * @file codea_test_module.cpp
 * @brief Codea primary regression-test module.
 *
 * Note - this module is not a good example of a simple Codea module.
 * See "codea_simple_module.cpp" for a simple Codea module.
 */

#include "codea_hookmap.h"
#include "codea_hooks.h"

#include "codea_test_module.h"


// codea_log.h must be included after codea_simple_module.h because of
// CODEA_PUBLISH_MODULE definition
#include "codea_log.h"


int CodeaTestHooks::Test_Post_Config( apr_pool_t* pconf, apr_pool_t* plog, apr_pool_t* ptmp, server_rec* s )
{
	CodeaLog log( s );
	log.Notice( "Inside Test_Post_Config" );

	return OK;
}

int CodeaTestHooks::Test_Open_Logs( apr_pool_t* pconf, apr_pool_t* plog, apr_pool_t* ptmp, server_rec* s )
{
	CodeaLog log( s );
	log.Notice( "Inside Test_Open_Logs" );

	return OK;
}

void CodeaTestHooks::Test_Child_Init( apr_pool_t* pchild, server_rec* s )
{
	CodeaLog log( s );
	log.Notice( "Inside Test_Child_Init" );
}

void CodeaTestHooks::Test_Insert_Filter( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Insert_Filter" );
}


/**
 * @brief Handler for the default_port hookpoint.
 * @param r - const pointer to the request_rec for this request.
 * @return Port considered to be the default communication port.
 *
 * Test_Default_Port also obtains a pointer to the CodeaTestHooks
 * class instance to invoke the base class behavior, in the same
 * fashion as CodeaTestHooks::Test_Http_Method.
 */
apr_port_t CodeaTestHooks::Test_Default_Port( const request_rec* r )
{
	CodeaLog log( r->pool );
	log.Notice( "Inside " );
	CodeaTestHooks* pInst = 0;
	pInst = CODEA_RETRIEVE_HOOK_CLASS( pInst, CodeaTestHooks::GetTag() );
	if ( pInst )
	{
		return pInst->DefaultPort( r );
	}
	return 0;
}

/**
 * @brief Returns the value for default_port from the base class.
 * @param r - const pointer to the request_rec for this request.
 * @return Port considered to be the default communication port.
 */
apr_port_t CodeaTestHooks::DefaultPort( const request_rec* r )
{
	return CodeaHooks::_Base_Default_Port( r );
}

int CodeaTestHooks::Test_Auth_Checker( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Auth_Checker" );
	return DECLINED;
}

int CodeaTestHooks::Test_Access_Checker( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Access_Checker" );
	return DECLINED;
}

int CodeaTestHooks::Test_Check_User_ID( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Check_User_ID" );
	return DECLINED;
}

int CodeaTestHooks::Test_Fixups( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Fixups" );
	return DECLINED;
}

/**
 * @brief Callback for the ap_hook_handler hookpoint.
 * @param r - pointer to request_rec for this request
 * @return Status of the request.
 *
 * Handling is declined unless the /codea_test uri is
 * requested.  In this case, data is sent to the output
 * and OK is returned.
 */
int CodeaTestHooks::Test_Handler( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Handler" );
	if ( 0 == ::strcmp( r->uri, "/codea_test" ) )
	{
		ap_rputs( "Inside the CodeaTest regression test module.", r );
		return OK;
	}
	return DECLINED;
}

int CodeaTestHooks::Test_Header_Parser( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Header_Parser" );
	return DECLINED;
}

int CodeaTestHooks::Test_Log_Transaction( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Log_Transaction" );
	return DECLINED;
}

int CodeaTestHooks::Test_Post_Read_Request( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Post_Read_Request" );
	return DECLINED;
}

int CodeaTestHooks::Test_Quick_Handler( request_rec* r , int lookup)
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Quick_Handler" );
	return DECLINED;
}

int CodeaTestHooks::Test_Translate_Name( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Translate_Name" );
	return DECLINED;
}

int CodeaTestHooks::Test_Type_Checker( request_rec* r )
{
	CodeaLog log( r );
	log.Notice( "Inside Test_Type_Checker" );
	return DECLINED;
}

int CodeaTestHooks::Test_Pre_Connection( conn_rec* c, void * v )
{
	CodeaLog log( c->pool );
	log.Notice( "Inside Test_Pre_Connection" );
	return DECLINED;
}

int CodeaTestHooks::Test_Process_Connection( conn_rec* c )
{
	CodeaLog log( c->pool );
	log.Notice( "Inside Test_Process_Connection" );
	return DECLINED;
}
