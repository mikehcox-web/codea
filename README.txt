/* ====================================================================
 * The Codea Open Source Project
 *
 * Codea stands for C++ Object-oriented Development Environment for
 * Apache.  Use Codea to build object-oriented applications on top of
 * the Apache 2.0 framework.
 *
 * Notes:
 * 1. Apache 2.0 must be installed and functional on your computer.
 *    Apache must be built with mod_so to allow dynamic shared objects.
 *    All Codea modules are built as .so modules that will be loaded
 *    at load time by Apache.
 * 2. Apache must be configured to load the C++ standard library.  This
 *    .so file must be loaded before any Codea modules are loaded.
 *    Example:
 *      # ... httpd.conf file
 *      # Erroneous way to load a Codea module
 *      LoadModule codea_module modules/codea_module.so
 *      LoadFile /usr/lib/libstdc++-libc6.2-2.so.3
 *    Example:
 *      # ... httpd.conf file
 *      # Correct way to load a Codea module
 *      LoadFile /usr/lib/libstdc++-libc6.2-2.so.3
 *      LoadModule codea_module modules/codea_module.so
 * 3. Use the supplied codea.mk file to help build Codea modules.
 *    All you need do is specify a value for APACHE_HOME and MODULE
 *    in your Makefile.  Optionally specify values for LOCAL_INCLUDES,
 *    LOCAL_LIBS, and LOCAL_CFLAGS.  Then include codea.mk and your
 *    module should build.
 * 4. Codea has only been tested on Red Hat Linux 7.1 so far.  Hey,
 *    it's new!  Codea should work without problems on most Unix
 *    platforms with no or minor modifications.  Windows is a
 *    different story...
 * 5. Codea modules are optimized during compilation (using the -O
 *    option) to minimize the size of the module, but you may run into
 *    out-of-memory issues, especially if loading several Codea modules
 *    and/or running on a system with less than 64 M of physical RAM.
 *    We're working on it; in the meantime, buy more RAM. ;)
 *
 * Look at codea_simple_module.cpp as an example of how to build a simple
 * Codea module.  Look at codea_test_module.cpp as a complete regression
 * of all Codea features.
 */ IMPLIED
