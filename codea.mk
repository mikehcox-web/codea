# ====================================================================
# The C++ Object-oriented Development Environment for Apache (Codea)
# Open Source Project License Version 1.0
#
# Copyright (c) 2001 The Codea Open Source Project.  All rights
# reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# 3. The end-user documentation included with the redistribution,
#    if any, must include the following acknowledgment:
#       "This product includes software developed by the
#        Codea Open Source Project and is based upon software
#        written by the Apache Software Foundation.  Codea is
#        not a derivative work of Apache nor is Codea endorsed
#        by the Apache Software Foundation."
#    Alternately, this acknowledgment may appear in the software itself,
#    if and wherever such third-party acknowledgments normally appear.
#
# 4. The names "Codea" and "Codea Open Source Project" must
#    not be used to endorse or promote products derived from this
#    software without prior written permission. For written
#    permission, please contact the project administrator.
#
# 5. Products derived from this software may not be called "Codea",
#    nor may "Codea" appear in their name, without prior written
#    permission.
#
# 6. All conditions set forth in the Apache Software Foundation
#    license must be met in addition to those set forth in this
#    license agreement.  For details regarding the Apache Software
#    Foundation license, please see http://www.apache.org/LICENSE.txt.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
# ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# ====================================================================


# codea.mk makefile template
# include this in your module's Makefile

APXS = $(APACHE_HOME)/bin/apxs
APXS_CC=`$(APXS) -q CC`   
APXS_TARGET=`$(APXS) -q TARGET`   
APXS_CFLAGS=`$(APXS) -q CFLAGS`   
APXS_SBINDIR=`$(APXS) -q SBINDIR`   
APXS_CFLAGS_SHLIB=`$(APXS) -q CFLAGS_SHLIB`   
APXS_INCLUDEDIR=`$(APXS) -q INCLUDEDIR`   
APXS_APR_INCLUDEDIR=`$(APXS) -q APR_INCLUDEDIR`
APXS_LD_SHLIB=`$(APXS) -q LD_SHLIB`
APXS_LIBEXECDIR=`$(APXS) -q LIBEXECDIR`
APXS_LDFLAGS_SHLIB=`$(APXS) -q LDFLAGS_SHLIB`
APXS_SYSCONFDIR=`$(APXS) -q SYSCONFDIR`
APXS_LIBS_SHLIB=`$(APXS) -q LIBS_SHLIB`

CC=g++
CHMOD=chmod
CP=cp -f
RM=rm -f


CODEA_INCLUDES = -I$(APXS_INCLUDEDIR) -I$(APXS_APR_INCLUDEDIR) -I$(CODEA_HOME)	\
-I$(CCL_HOME) -I. $(LOCAL_INCLUDES)
CODEA_LIBS = $(CODEA_HOME)/codea_hooks.o $(APXS_LIBS_SHLIB) $(LOCAL_LIBS)
CODEA_CFLAGS = -Wall -fPIC $(APXS_CFLAGS) $(APXS_CFLAGS_SHLIB) $(LOCAL_CFLAGS)

.SUFFIXES:	.o .cpp .so
.cpp.o:	$<
	$(CC) -c -O $(CFLAGS) $(CODEA_CFLAGS) $(CODEA_INCLUDES) -o $@ $<
.o.so:	$<
	$(CC) -shared $(CFLAGS) $(CODEA_CFLAGS) -o $@ $< $(CODEA_LIBS)

all:	$(CODEA_HOME)/codea_hooks.o $(MODULE)

install:	all
	$(CHMOD) 755 *.so
	$(CP) *.so $(APXS_LIBEXECDIR)

clean:
	$(RM) *.so *.o *~

check_apxs_vars:
	@echo APXS_CC $(APXS_CC);\
	echo APXS_TARGET $(APXS_TARGET);\
	echo APXS_CFLAGS $(APXS_CFLAGS);\
	echo APXS_SBINDIR $(APXS_SBINDIR);\
	echo APXS_CFLAGS_SHLIB $(APXS_CFLAGS_SHLIB);\
	echo APXS_INCLUDEDIR $(APXS_INCLUDEDIR);\
	echo APXS_LD_SHLIB $(APXS_LD_SHLIB);\
	echo APXS_LIBEXECDIR $(APXS_LIBEXECDIR);\
	echo APXS_LDFLAGS_SHLIB $(APXS_LDFLAGS_SHLIB);\
	echo APXS_SYSCONFDIR $(APXS_SYSCONFDIR);\
	echo APXS_LIBS_SHLIB $(APXS_LIBS_SHLIB)

