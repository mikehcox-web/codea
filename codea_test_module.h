/* ====================================================================
 * The C++ Object-oriented Development Environment for Apache (Codea)
 * Open Source Project License Version 1.0
 *
 * Copyright (c) 2001 The Codea Open Source Project.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Codea Open Source Project and is based upon software
 *        written by the Apache Software Foundation.  Codea is
 *        not a derivative work of Apache nor is Codea endorsed
 *        by the Apache Software Foundation."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Codea" and "Codea Open Source Project" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact the project administrator.
 *
 * 5. Products derived from this software may not be called "Codea",
 *    nor may "Codea" appear in their name, without prior written
 *    permission.
 *
 * 6. All conditions set forth in the Apache Software Foundation
 *    license must be met in addition to those set forth in this
 *    license agreement.  For details regarding the Apache Software
 *    Foundation license, please see http://www.apache.org/LICENSE.txt.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 */

#ifndef __CODEA_TEST_MODULE_H__
#define __CODEA_TEST_MODULE_H__

static const CodeaHooks::tag_t& CodeaTest_tag( "CodeaTestHooks" );

/**
 * Subclass of CodeaHooks for the regression test module.
 *
 * We define a static method for every supported hookpoint
 * and register each one with the corresponding callback.
 */
class CodeaTestHooks : public CodeaHooks
{
public:
	/** Return the tag for this CodeaHooks subclass. */
	static const tag_t& GetTag() { return CodeaTest_tag; }

	static int Test_Post_Config( apr_pool_t* pconf, apr_pool_t* plog, apr_pool_t* ptmp, server_rec* s );
	static int Test_Open_Logs( apr_pool_t* pconf, apr_pool_t* plog, apr_pool_t* ptmp, server_rec* s );

	static void Test_Child_Init( apr_pool_t* pchild, server_rec* s );

	static void Test_Insert_Filter( request_rec* r );

	static void Test_Optional_Fn_Retrieve( void ){}

	static apr_port_t Test_Default_Port( const request_rec* r );
	apr_port_t DefaultPort( const request_rec* r );

	static int Test_Auth_Checker( request_rec* r );
	static int Test_Access_Checker( request_rec* r );
	static int Test_Check_User_ID( request_rec* r );
	static int Test_Fixups( request_rec* r );
	static int Test_Handler( request_rec* r );
	static int Test_Header_Parser( request_rec* r );
	static int Test_Log_Transaction( request_rec* r );
	static int Test_Post_Read_Request( request_rec* r );
        static int Test_Quick_Handler( request_rec* r, int lookup);
	static int Test_Translate_Name( request_rec* r );
	static int Test_Type_Checker( request_rec* r );

        static int Test_Pre_Connection( conn_rec* c, void * );
	static int Test_Process_Connection( conn_rec* c );
};

CODEA_REGISTER_HOOK_CLASS( CodeaTestHooks, CodeaTestHooks::GetTag() );

CODEA_BEGIN_HOOK_MAP( codea_test )
CODEA_POST_CONFIG( CodeaTestHooks::Test_Post_Config, 0, 0, APR_HOOK_MIDDLE )
CODEA_OPEN_LOGS( CodeaTestHooks::Test_Open_Logs, 0, 0, APR_HOOK_MIDDLE )
CODEA_CHILD_INIT( CodeaTestHooks::Test_Child_Init, 0, 0, APR_HOOK_MIDDLE )
CODEA_INSERT_FILTER( CodeaTestHooks::Test_Insert_Filter, 0, 0, APR_HOOK_MIDDLE )
CODEA_OPTIONAL_FN_RETRIEVE( CodeaTestHooks::Test_Optional_Fn_Retrieve, 0, 0, APR_HOOK_MIDDLE )
CODEA_DEFAULT_PORT( CodeaTestHooks::Test_Default_Port, 0, 0, APR_HOOK_MIDDLE )
CODEA_AUTH_CHECKER( CodeaTestHooks::Test_Auth_Checker, 0, 0, APR_HOOK_MIDDLE )
CODEA_ACCESS_CHECKER( CodeaTestHooks::Test_Access_Checker, 0, 0, APR_HOOK_MIDDLE )
CODEA_CHECK_USER_ID( CodeaTestHooks::Test_Check_User_ID, 0, 0, APR_HOOK_MIDDLE )
CODEA_FIXUPS( CodeaTestHooks::Test_Fixups, 0, 0, APR_HOOK_MIDDLE )
CODEA_HANDLER( CodeaTestHooks::Test_Handler, 0, 0, APR_HOOK_MIDDLE )
CODEA_HEADER_PARSER( CodeaTestHooks::Test_Header_Parser, 0, 0, APR_HOOK_MIDDLE )
CODEA_LOG_TRANSACTION( CodeaTestHooks::Test_Log_Transaction, 0, 0, APR_HOOK_MIDDLE )
CODEA_POST_READ_REQUEST( CodeaTestHooks::Test_Post_Read_Request, 0, 0, APR_HOOK_MIDDLE )
CODEA_QUICK_HANDLER( CodeaTestHooks::Test_Quick_Handler, 0, 0, APR_HOOK_MIDDLE )
CODEA_TRANSLATE_NAME( CodeaTestHooks::Test_Translate_Name, 0, 0, APR_HOOK_MIDDLE )
CODEA_TYPE_CHECKER( CodeaTestHooks::Test_Type_Checker, 0, 0, APR_HOOK_MIDDLE )
CODEA_PRE_CONNECTION( CodeaTestHooks::Test_Pre_Connection, 0, 0, APR_HOOK_MIDDLE )
CODEA_PROCESS_CONNECTION( CodeaTestHooks::Test_Process_Connection, 0, 0, APR_HOOK_MIDDLE )
CODEA_END_HOOK_MAP( codea_test )

CODEA_PUBLISH_MODULE( codea_test, 0, 0, 0, 0, 0 )



#endif // __CODEA_TEST_MODULE_H__
