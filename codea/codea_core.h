/* ====================================================================
 * The C++ Object-oriented Development Environment for Apache (Codea)
 * Open Source Project License Version 1.0
 *
 * Copyright (c) 2001 The Codea Open Source Project.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Codea Open Source Project and is based upon software
 *        written by the Apache Software Foundation.  Codea is
 *        not a derivative work of Apache nor is Codea endorsed
 *        by the Apache Software Foundation."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Codea" and "Codea Open Source Project" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact the project administrator.
 *
 * 5. Products derived from this software may not be called "Codea",
 *    nor may "Codea" appear in their name, without prior written
 *    permission.
 *
 * 6. All conditions set forth in the Apache Software Foundation
 *    license must be met in addition to those set forth in this
 *    license agreement.  For details regarding the Apache Software
 *    Foundation license, please see http://www.apache.org/LICENSE.txt.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 */

#ifndef __CODEA_CORE_H__
#define __CODEA_CORE_H__

extern "C" {
#include "httpd.h"
#include "http_config.h"
#include "http_log.h"
#include "http_request.h"
#include "http_protocol.h"
#include "http_connection.h"
#include "ap_compat.h"
}

/**
 * @brief Create the module structure for your Codea module.
 * @param mod_name - The name of the module (no quotes).  The suffix _module will be added to this name.
 * @param dircfg_create - The name of the per-directory configuration creation routine (or 0/NULL)
 * @param dircfg_merge - The name of the per-directory configuration merge routine (or 0/NULL)
 * @param svrcfg_create - The name of the per-server configuration creation routine (or 0/NULL)
 * @param svrcfg_merge - The name of the per-server configuration merge routine (or 0/NULL)
 * @param cmds - The name of the command_rec structure for configuration mappings
 * @see CODEA_BEGIN_HOOK_MAP
 * @see CODEA_END_HOOK_MAP
 *
 * This macro creates the module structure declaration for your module.  Use in conjuction with
 * CODEA_BEGIN_HOOK_MAP and CODEA_END_HOOK_MAP.
 *
 * Example:
 *
 * CODEA_PUBLISH_MODULE( my_test, 0, 0, 0, 0, 0 ) // module with no cfg.
 *
 * CODEA_PUBLISH_MODULE( my_test2, cfg_creator, cfg_merger, 0, 0, cfg_cmds ) // module w/ per-directory cfg.
 * 
 */
#define CODEA_PUBLISH_MODULE( mod_name, dircfg_create, dircfg_merge, svrcfg_create, svrcfg_merge, cmds ) \
AP_DECLARE_MODULE(mod_name) = {				\
STANDARD20_MODULE_STUFF, \
dircfg_create, \
dircfg_merge, \
svrcfg_create, \
svrcfg_merge, \
cmds, \
mod_name##_register_hooks \
};

#endif // __CODEA_CORE_H__
