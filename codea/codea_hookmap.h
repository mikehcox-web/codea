/* ====================================================================
 * The C++ Object-oriented Development Environment for Apache (Codea)
 * Open Source Project License Version 1.0
 *
 * Copyright (c) 2001 The Codea Open Source Project.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Codea Open Source Project and is based upon software
 *        written by the Apache Software Foundation.  Codea is
 *        not a derivative work of Apache nor is Codea endorsed
 *        by the Apache Software Foundation."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Codea" and "Codea Open Source Project" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact the project administrator.
 *
 * 5. Products derived from this software may not be called "Codea",
 *    nor may "Codea" appear in their name, without prior written
 *    permission.
 *
 * 6. All conditions set forth in the Apache Software Foundation
 *    license must be met in addition to those set forth in this
 *    license agreement.  For details regarding the Apache Software
 *    Foundation license, please see http://www.apache.org/LICENSE.txt.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 */

#ifndef __CODEA_HOOK_MAP_H__
#define __CODEA_HOOK_MAP_H__

/**
 * @file codea_hookmap.h
 * @brief Facilities to enable the creation of automated hookmap-callback mappings.
 */

#include "codea_core.h"

/**
 * Stores information pertaining to an Apache hook callback.  Instances
 * of this class are created automatically by the macros that are
 * included within the CODEA_BEGIN_HOOK_MAP and CODEA_END_HOOK_MAP macros.
 */
class CodeaHookMapEntry
{
private:
	typedef const char * const * csa_t;  /**< Const string array type - used to identify predecessors and successors for callbacks. */

    /**
     * Enumerated type used to identify the type of hookpoint represented by this object.
     */
    typedef enum
    {
	VPPPS,   /**< Identifies hookpoint with void callback function taking apr_pool_t*, apr_pool_t*, apr_pool_t* and server_rec* */
	VPS,     /**< Identifies hookpoint with void callback function taking apr_pool_t* and server_rec* */
	VR,      /**< Identifies hookpoint with void callback function taking request_rec* */
	VV,      /**< Identifies hookpoint with void callback function taking no parameters (void) */
	CCR,     /**< Identifies hookpoint with const char* callback function taking const request_rec* */
	PCR,     /**< Identifies hookpoint with apr_port_t callback function taking const request_rec* */
	IR,      /**< Identifies hookpoint with int callback function taking request_rec* */
	IC,      /**< Identifies hookpoint with int callback function taking conn_rec* */
	UNKNOWN  /**< flag for unknown/invalid object */
    } hooktype_t;

    typedef int         (*vppps_callback_t) ( apr_pool_t*, apr_pool_t*, apr_pool_t*, server_rec* ); /**< Type for void callback function taking apr_pool_t*, apr_pool_t*, apr_pool_t*, and server_rec* */
    typedef void         (*vps_callback_t)   ( apr_pool_t*, server_rec* );                           /**< Type for void callback function taking apr_pool_t* and server_rec* */
    typedef void         (*vr_callback_t)    ( request_rec* );                                       /**< Type for void callback function taking request_rec* */
    typedef void         (*vv_callback_t)    ( void );                                               /**< Type for void callback function taking no parameters (void) */
    typedef const char * (*ccr_callback_t)   ( const request_rec* );                                 /**< Type for const char* callback function taking const request_rec* */
    typedef apr_port_t   (*pcr_callback_t)   ( const request_rec* );                                 /**< Type for apr_port_t callback function taking const request_rec* */
    typedef int          (*ir_callback_t)    ( request_rec* );                                       /**< Type for int callback function taking request_rec* */
    typedef int          (*iri_callback_t)   ( request_rec* , int);                                  /**< Type for int callback function taking request_rec* and int */    
    typedef int          (*ic_callback_t)    ( conn_rec* );                                          /**< Type for int callback function taking conn_rec* */
    typedef int          (*icv_callback_t)   ( conn_rec* , void*);                                   /**< Type for int callback function taking conn_rec* and void* */    

    typedef void (*vppps_hookpoint_t) ( vppps_callback_t, csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a vppps_callback_t */
    typedef void (*vps_hookpoint_t)   ( vps_callback_t,   csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a vps_callback_t */
    typedef void (*vr_hookpoint_t)    ( vr_callback_t,    csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a vr_callback_t */
    typedef void (*vv_hookpoint_t)    ( vv_callback_t,    csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a vv_callback_t */
    typedef void (*ccr_hookpoint_t)   ( ccr_callback_t,   csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a ccr_callback_t */
    typedef void (*pcr_hookpoint_t)   ( pcr_callback_t,   csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a pcr_callback_t */
    typedef void (*ir_hookpoint_t)    ( ir_callback_t,    csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a ir_callback_t */
    typedef void (*iri_hookpoint_t)   ( iri_callback_t,   csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a iri_callback_t */
    typedef void (*ic_hookpoint_t)    ( ic_callback_t,    csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a ic_callback_t */
    typedef void (*icv_hookpoint_t)   ( icv_callback_t,   csa_t, csa_t, int ); /**< Type for Apache hookpoint taking a icv_callback_t */  

    hooktype_t m_Type; /**< The type of hookpoint represented by this object. */

    /**
     * Anonymous union to contain the pointer to the Apache hookpoint function to be used for this entry.
     */
    union
    {
	vppps_hookpoint_t vpppsh;
	vps_hookpoint_t   vpsh;
	vr_hookpoint_t    vrh;
	vv_hookpoint_t    vvh;
	ccr_hookpoint_t   ccrh;
	pcr_hookpoint_t   pcrh;
	ir_hookpoint_t    irh;
	iri_hookpoint_t   irih;
	ic_hookpoint_t    ich;
	icv_hookpoint_t   icvh;      
	void*             h_undef;
    };

    /**
     * Anonymous union to contain the pointer to the local callback function to be used for this entry.
     */
    union
    {
	vppps_callback_t vpppsc;
	vps_callback_t   vpsc;
	vr_callback_t    vrc;
	vv_callback_t    vvc;
	ccr_callback_t   ccrc;
	pcr_callback_t   pcrc;
	ir_callback_t    irc;
	iri_callback_t    iric;      
	ic_callback_t    icc;
	icv_callback_t   icvc;      
	void*            c_undef;
    };
	
	csa_t m_ppsPredecessors; /**< Const string array of predecessors for this callback. */
	csa_t m_ppsSuccessors;   /**< Const string array of successors for this callback. */
	int m_Position;          /**< Hook position - one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST */
	bool m_bValid;           /**< Indicates whether this object represents a valid callback or not. */

public:
	/**
	 * Default constructor creates an invalid object - used to mark the end of the array.
	 */
	CodeaHookMapEntry()
		:
		m_Type( UNKNOWN ),
		h_undef( 0 ),
		c_undef( 0 ),
		m_ppsPredecessors(0),
		m_ppsSuccessors(0),
		m_Position( -1 ),
		m_bValid( false )
	{}

	/**
	 * Create an object representing a hookpoint of type VPPPS.
	 * @param fphp - function pointer to hookpoint
	 * @param fpcb - function pointer to callback
	 * @param pre - list of predecessor modules to the callback
	 * @param post - list of successsor modules to the callback
	 * @param pos - hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
	 * @see VPPPS, vppps_hookpoint_t, vppps_callback_t, csa_t
	 *
	 * This is the constructor used for hookpoints ap_hook_post_config and ap_hook_open_logs.
	 */
	CodeaHookMapEntry(
		vppps_hookpoint_t fphp,
		vppps_callback_t fpcb,
		csa_t pre, csa_t suc, int pos
		)
		:
		m_Type( VPPPS ),
		vpppsh( fphp ),
		vpppsc( fpcb ),
		m_ppsPredecessors( pre ),
		m_ppsSuccessors( suc ),
		m_Position( pos ),
		m_bValid( true )
	{}

	/**
	 * Create an object representing a hookpoint of type VPS.
	 * @param fphp - function pointer to hookpoint
	 * @param fpcb - function pointer to callback
	 * @param pre - list of predecessor modules to the callback
	 * @param post - list of successsor modules to the callback
	 * @param pos - hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
	 * @see VPS, vps_hookpoint_t, vps_callback_t, csa_t
	 *
	 * This is the constructor used for hookpoint ap_hook_child_init.
	 */
	CodeaHookMapEntry(
		vps_hookpoint_t fphp,
		vps_callback_t fpcb,
		csa_t pre, csa_t suc, int pos
		)
		:
		m_Type( VPS ),
		vpsh( fphp ),
		vpsc( fpcb ),
		m_ppsPredecessors( pre ),
		m_ppsSuccessors( suc ),
		m_Position( pos ),
		m_bValid( true )
	{}

	/**
	 * Create an object representing a hookpoint of type VR.
	 * @param fphp - function pointer to hookpoint
	 * @param fpcb - function pointer to callback
	 * @param pre - list of predecessor modules to the callback
	 * @param post - list of successsor modules to the callback
	 * @param pos - hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
	 * @see VR, vr_hookpoint_t, vr_callback_t, csa_t
	 *
	 * This is the constructor used for hookpoint ap_hook_insert_filter.
	 */
	CodeaHookMapEntry(
		vr_hookpoint_t fphp,
		vr_callback_t fpcb,
		csa_t pre, csa_t suc, int pos
		)
		:
		m_Type( VR ),
		vrh( fphp ),
		vrc( fpcb ),
		m_ppsPredecessors( pre ),
		m_ppsSuccessors( suc ),
		m_Position( pos ),
		m_bValid( true )
	{}

	/**
	 * Create an object representing a hookpoint of type VV.
	 * @param fphp - function pointer to hookpoint
	 * @param fpcb - function pointer to callback
	 * @param pre - list of predecessor modules to the callback
	 * @param post - list of successsor modules to the callback
	 * @param pos - hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
	 * @see VV, vv_hookpoint_t, vv_callback_t, csa_t
	 *
	 * This is the constructor used for hookpoint ap_hook_optional_fn_retrieve.
	 */
	CodeaHookMapEntry(
		vv_hookpoint_t fphp,
		vv_callback_t fpcb,
		csa_t pre, csa_t suc, int pos
		)
		:
		m_Type( VV ),
		vvh( fphp ),
		vvc( fpcb ),
		m_ppsPredecessors( pre ),
		m_ppsSuccessors( suc ),
		m_Position( pos ),
		m_bValid( true )
	{}

    /**
     * Create an object representing a hookpoint of type PCR.
     * @param fphp - function pointer to hookpoint
     * @param fpcb - function pointer to callback
     * @param pre - list of predecessor modules to the callback
     * @param post - list of successsor modules to the callback
     * @param pos - hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
     * @see PCR, pcr_hookpoint_t, pcr_callback_t, csa_t
     *
     * This is the constructor used for hookpoint ap_hook_default_port.
     */
    CodeaHookMapEntry(
	pcr_hookpoint_t fphp,
	pcr_callback_t fpcb,
	csa_t pre, csa_t suc, int pos
	)
	:
	m_Type( PCR ),
	pcrh( fphp ),
	pcrc( fpcb ),
	m_ppsPredecessors( pre ),
	m_ppsSuccessors( suc ),
	m_Position( pos ),
	m_bValid( true )
	{}

    /**
     * Create an object representing a hookpoint of type IR.
     * @param fphp - function pointer to hookpoint
     * @param fpcb - function pointer to callback
     * @param pre - list of predecessor modules to the callback
     * @param post - list of successsor modules to the callback
     * @param pos - hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
     * @see IR, ir_hookpoint_t, ir_callback_t, csa_t
     *
     * This is the constructor used for the following hookpoints:
     *   - ap_hook_auth_checker
     *   - ap_hook_access_checker
     *   - ap_hook_check_user_id
     *   - ap_hook_create_request
     *   - ap_hook_fixups
     *   - ap_hook_handler
     *   - ap_hook_header_parser
     *   - ap_hook_log_transaction
     *   - ap_hook_post_read_request
     *   - ap_hook_translate_name
     *   - ap_hook_type_checker
     */
    CodeaHookMapEntry(
	ir_hookpoint_t fphp,
	ir_callback_t fpcb,
	csa_t pre, csa_t suc, int pos
	)
	:
	m_Type( IR ),
	irh( fphp ),
	irc( fpcb ),
	m_ppsPredecessors( pre ),
	m_ppsSuccessors( suc ),
	m_Position( pos ),
	m_bValid( true )
	{}

    /**
     * Create an object representing a hookpoint of type IR.
     * @param fphp - function pointer to hookpoint
     * @param fpcb - function pointer to callback
     * @param pre - list of predecessor modules to the callback
     * @param post - list of successsor modules to the callback
     * @param pos - hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
     * @see IR, ir_hookpoint_t, ir_callback_t, csa_t
     *
     * This is the constructor used for the following hookpoints:
     *   - ap_hook_quick_handler
     */
    CodeaHookMapEntry(
	iri_hookpoint_t fphp,
	iri_callback_t fpcb,
	csa_t pre, csa_t suc, int pos
	)
	:
	m_Type( IR ),
	irih( fphp ),
	iric( fpcb ),
	m_ppsPredecessors( pre ),
	m_ppsSuccessors( suc ),
	m_Position( pos ),
	m_bValid( true )
	{}

    
    /**
     * Create an object representing a hookpoint of type IC.
     * @param fphp - function pointer to hookpoint
     * @param fpcb - function pointer to callback
     * @param pre - list of predecessor modules to the callback
     * @param post - list of successsor modules to the callback
     * @param pos - hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
     * @see IC, ic_hookpoint_t, ic_callback_t, csa_t
     *
     * This is the constructor used for hookpoints ap_hook_process_connection.
     */
    CodeaHookMapEntry(
	ic_hookpoint_t fphp,
	ic_callback_t fpcb,
	csa_t pre, csa_t suc, int pos
	)
	:
	m_Type( IC ),
	ich( fphp ),
	icc( fpcb ),
	m_ppsPredecessors( pre ),
	m_ppsSuccessors( suc ),
	m_Position( pos ),
	m_bValid( true )
	{}


    /**
     * Create an object representing a hookpoint of type IC.
     * @param fphp - function pointer to hookpoint
     * @param fpcb - function pointer to callback
     * @param pre - list of predecessor modules to the callback
     * @param post - list of successsor modules to the callback
     * @param pos - hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
     * @see IC, ic_hookpoint_t, ic_callback_t, csa_t
     *
     * This is the constructor used for hookpoints ap_hook_pre_connection 
     */
    CodeaHookMapEntry(
	icv_hookpoint_t fphp,
	icv_callback_t fpcb,
	csa_t pre, csa_t suc, int pos
	)
	:
	m_Type( IC ),
	icvh( fphp ),
	icvc( fpcb ),
	m_ppsPredecessors( pre ),
	m_ppsSuccessors( suc ),
	m_Position( pos ),
	m_bValid( true )
	{}

	/**
	 * Determine whether this object represents a valid hook mapping.
	 * @return state, true means the object is valid
	 *
	 * This state is set at construction time automatically.
	 */
	bool IsValid()
	{
		return m_bValid;
	}

	/**
	 * Invoke the Apache hook function with the supplied parameters,
	 * depending on the hook type specified within the object.
	 */
	void InvokeApacheHook()
	{
		switch( m_Type )
		{
		case VPPPS:
			vpppsh( vpppsc, m_ppsPredecessors, m_ppsSuccessors, m_Position );
			break;
		case VPS:
			vpsh( vpsc, m_ppsPredecessors, m_ppsSuccessors, m_Position );
			break;
		case VR:
			vrh( vrc, m_ppsPredecessors, m_ppsSuccessors, m_Position );
			break;
		case VV:
			vvh( vvc, m_ppsPredecessors, m_ppsSuccessors, m_Position );
			break;
		case CCR:
			ccrh( ccrc, m_ppsPredecessors, m_ppsSuccessors, m_Position );
			break;
		case PCR:
			pcrh( pcrc, m_ppsPredecessors, m_ppsSuccessors, m_Position );
			break;
		case IR:
			irh( irc, m_ppsPredecessors, m_ppsSuccessors, m_Position );
			break;
		case IC:
			ich( icc, m_ppsPredecessors, m_ppsSuccessors, m_Position );
			break;
		case UNKNOWN:
		default:
			break;
		}
	}
};

/**
 * @def CODEA_BEGIN_HOOK_MAP
 * @brief Identifies the beginning of the hook map.
 * @param mod_name - the name of the module, no quotes
 * 
 * Usage:  CODEA_BEGIN_HOOK_MAP( codea_test )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_END_HOOK_MAP
 * @brief Identifies the end of the hook map.
 * @param mod_name - the name of the module, no quotes
 * 
 * Usage:  CODEA_END_HOOK_MAP( codea_test )
 *
 * Automatically includes an invalid (equivalent of null) entry,
 * terminates the map, and creates the appropriate register_hooks
 * function for this module to invoke all the hookpoints.
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_POST_CONFIG
 * @brief Adds a hookpoint-callback mapping to the hook map for the post configuration phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_POST_CONFIG( postcfg_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_POST_CONFIG( postcfg_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_OPEN_LOGS
 * @brief Adds a hookpoint-callback mapping to the hook map for the log opening phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_OPEN_LOGS( openlogs_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_OPEN_LOGS( openlogs_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_CHILD_INIT
 * @brief Adds a hookpoint-callback mapping to the hook map for the child initialization phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_CHILD_INIT( childinit_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_CHILD_INIT( childinit_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_INSERT_FILTER
 * @brief Adds a hookpoint-callback mapping to the hook map for the insert filter phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_INSERT_FILTER( insert_filter_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_INSERT_FILTER( insert_filter_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_OPTIONAL_FN_RETRIEVE
 * @brief Adds a hookpoint-callback mapping to the hook map for the optional fn retrieve phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_OPTIONAL_FN_RETRIEVE( ofr_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_OPTIONAL_FN_RETRIEVE( ofr_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_HTTP_METHOD
 * @brief Adds a hookpoint-callback mapping to the hook map for the HTTP method retrieval phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_HTTP_METHOD( http_method_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_HTTP_METHOD( http_method_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_DEFAULT_PORT
 * @brief Adds a hookpoint-callback mapping to the hook map for the default port retrieval phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_DEFAULT_PORT( defport_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_DEFAULT_PORT( defport_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_AUTH_CHECKER
 * @brief Adds a hookpoint-callback mapping to the hook map for the authentication phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_AUTH_CHECKER( auth_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_AUTH_CHECKER( auth_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_ACCESS_CHECKER
 * @brief Adds a hookpoint-callback mapping to the hook map for the access checking phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_ACCESS_CHECKER( access_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_ACCESS_CHECKER( access_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_CHECK_USER_ID
 * @brief Adds a hookpoint-callback mapping to the hook map for the user id checking phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_CHECK_USER_ID( check_userid_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_CHECK_USER_ID( check_userid_callback, predecessor_list, 0, APR_HOOK_LAST )
 *
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_CREATE_REQUEST
 * @brief Adds a hookpoint-callback mapping to the hook map for the request creation phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_CREATE_REQUEST( create_request_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_CREATE_REQUEST( create_request_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 *
 * Note: This phase is currently disabled until I figure out how to make it work. -MR
 */

/**
 * @def CODEA_FIXUPS
 * @brief Adds a hookpoint-callback mapping to the hook map for the fixup phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_FIXUPS( fixup_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_FIXUPS( fixup_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_HANDLER
 * @brief Adds a hookpoint-callback mapping to the hook map for the handler phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Also known as the content generation phase, this is the phase where the module creates
 * the data to return.
 *
 * Usage:
 * 
 * CODEA_HANDLER( handler_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_HANDLER( handler_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_HEADER_PARSER
 * @brief Adds a hookpoint-callback mapping to the hook map for the header parsing phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Most modules will use post_read_request to obtain header information instead of this one.
 *
 * Usage:
 * 
 * CODEA_HEADER_PARSER( header_parser_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_HEADER_PARSER( header_parser_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_LOG_TRANSACTION
 * @brief Adds a hookpoint-callback mapping to the hook map for the log transaction phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_LOG_TRANSACTION( logtrans_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_LOG_TRANSACTION( logtrans_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_POST_READ_REQUEST
 * @brief Adds a hookpoint-callback mapping to the hook map for the post read request phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Called just after the request is read, before other hookpoints.
 *
 * Usage:
 * 
 * CODEA_POST_READ_REQUEST( prr_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_POST_READ_REQUEST( prr_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_QUICK_HANDLER
 * @brief Adds a hookpoint-callback mapping to the hook map for the quick handler phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_QUICK_HANDLER( qh_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_QUICK_HANDLER( qh_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_TRANSLATE_NAME
 * @brief Adds a hookpoint-callback mapping to the hook map for the translation phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * URL translation is done here.
 *
 * Usage:
 * 
 * CODEA_TRANSLATE_NAME( trans_name_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_TRANSLATE_NAME( trans_name_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_TYPE_CHECKER
 * @brief Adds a hookpoint-callback mapping to the hook map for the type checking phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * MIME type determination is done here.
 *
 * Usage:
 * 
 * CODEA_TYPE_CHECKER( type_check_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_TYPE_CHECKER( type_check_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_PRE_CONNECTION
 * @brief Adds a hookpoint-callback mapping to the hook map for the pre-connection phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_PRE_CONNECTION( pre_conn_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_PRE_CONNECTION( pre_conn_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_PROCESS_CONNECTION
 * @brief Adds a hookpoint-callback mapping to the hook map for the process-connection phase.
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Usage:
 * 
 * CODEA_PROCESS_CONNECTION( proc_conn_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_PROCESS_CONNECTION( proc_conn_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

/**
 * @def CODEA_CUSTOM_CALLBACK
 * @brief Adds a hookpoint-callback mapping to the hook map for a custom callback.
 * @param hp - Custom hookpoint to use
 * @param cb - Callback function for this hookpoint
 * @param pre - Null-terminated array of strings of predecessor modules (or NULL/0)
 * @param suc - Null-terminated array of strings of successor modules (or NULL/0)
 * @param pos - Hook position (one of APR_HOOK_FIRST, APR_HOOK_MIDDLE, or APR_HOOK_LAST)
 *
 * Use this macro to generate an entry for your custom callback.  As long as your callback
 * matches a type defined in CodeaHookMapEntry, you should be able to generate the mapping
 * automatically.  If not, you should subclass CodeaHookMapEntry to add a constructor that
 * supports the type you need.
 *
 * Usage:
 * 
 * CODEA_CUSTOM_CALLBACK( my_custom_hookpoint, my_custom_callback, 0, 0, APR_HOOK_MIDDLE )
 *
 * CODEA_CUSTOM_CALLBACK( my_custom_hookpoint, my_custom_callback, predecessor_list, 0, APR_HOOK_LAST )
 * 
 * Note - no semicolon at the end.
 */

#define CODEA_BEGIN_HOOK_MAP( mod_name ) CodeaHookMapEntry mod_name##_hook_map_entry [] = {
#define CODEA_END_HOOK_MAP( mod_name ) CodeaHookMapEntry() \
}; \
static void mod_name##_register_hooks( apr_pool_t* p ) { \
for ( CodeaHookMapEntry* pEnt = &mod_name##_hook_map_entry[0]; pEnt->IsValid(); ++pEnt ) \
	pEnt->InvokeApacheHook(); \
}

#define CODEA_POST_CONFIG(cb,pre,suc,pos)          CodeaHookMapEntry( ap_hook_post_config, cb, pre, suc, pos ),
#define CODEA_OPEN_LOGS(cb,pre,suc,pos)            CodeaHookMapEntry( ap_hook_open_logs, cb, pre, suc, pos ),
#define CODEA_CHILD_INIT(cb,pre,suc,pos)           CodeaHookMapEntry( ap_hook_child_init, cb, pre, suc, pos ),
#define CODEA_INSERT_FILTER(cb,pre,suc,pos)        CodeaHookMapEntry( ap_hook_insert_filter, cb, pre, suc, pos ),
#define CODEA_OPTIONAL_FN_RETRIEVE(cb,pre,suc,pos) CodeaHookMapEntry( ap_hook_optional_fn_retrieve, cb, pre, suc, pos ),
#define CODEA_HTTP_METHOD(cb,pre,suc,pos)          CodeaHookMapEntry( ap_hook_http_method, cb, pre, suc, pos ),
#define CODEA_DEFAULT_PORT(cb,pre,suc,pos)         CodeaHookMapEntry( ap_hook_default_port, cb, pre, suc, pos ),
#define CODEA_AUTH_CHECKER(cb,pre,suc,pos)         CodeaHookMapEntry( ap_hook_auth_checker, cb, pre, suc, pos ),
#define CODEA_ACCESS_CHECKER(cb,pre,suc,pos)       CodeaHookMapEntry( ap_hook_access_checker, cb, pre, suc, pos ),
#define CODEA_CHECK_USER_ID(cb,pre,suc,pos)        CodeaHookMapEntry( ap_hook_check_user_id, cb, pre, suc, pos ),
//#define CODEA_CREATE_REQUEST(cb,pre,suc,pos)       CodeaHookMapEntry( ap_hook_create_request, cb, pre, suc, pos ),
#define CODEA_FIXUPS(cb,pre,suc,pos)               CodeaHookMapEntry( ap_hook_fixups, cb, pre, suc, pos ),
#define CODEA_HANDLER(cb,pre,suc,pos)              CodeaHookMapEntry( ap_hook_handler, cb, pre, suc, pos ),
#define CODEA_HEADER_PARSER(cb,pre,suc,pos)        CodeaHookMapEntry( ap_hook_header_parser, cb, pre, suc, pos ),
#define CODEA_LOG_TRANSACTION(cb,pre,suc,pos)      CodeaHookMapEntry( ap_hook_log_transaction, cb, pre, suc, pos ),
#define CODEA_POST_READ_REQUEST(cb,pre,suc,pos)    CodeaHookMapEntry( ap_hook_post_read_request, cb, pre, suc, pos ),
#define CODEA_QUICK_HANDLER(cb,pre,suc,pos)        CodeaHookMapEntry( ap_hook_quick_handler, cb, pre, suc, pos ),
#define CODEA_TRANSLATE_NAME(cb,pre,suc,pos)       CodeaHookMapEntry( ap_hook_translate_name, cb, pre, suc, pos ),
#define CODEA_TYPE_CHECKER(cb,pre,suc,pos)         CodeaHookMapEntry( ap_hook_type_checker, cb, pre, suc, pos ),
#define CODEA_PRE_CONNECTION(cb,pre,suc,pos)       CodeaHookMapEntry( ap_hook_pre_connection, cb, pre, suc, pos ),
#define CODEA_PROCESS_CONNECTION(cb,pre,suc,pos)   CodeaHookMapEntry( ap_hook_process_connection, cb, pre, suc, pos ),
#define CODEA_CUSTOM_CALLBACK(hp,cb,pre,suc,pos)   CodeaHookMapEntry( hp, cb, pre, suc, pos ),


#endif // __CODEA_HOOK_MAP_H__
