/* ====================================================================
 * The C++ Object-oriented Development Environment for Apache (Codea)
 * Open Source Project License Version 1.0
 *
 * Copyright (c) 2001 The Codea Open Source Project.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Codea Open Source Project and is based upon software
 *        written by the Apache Software Foundation.  Codea is
 *        not a derivative work of Apache nor is Codea endorsed
 *        by the Apache Software Foundation."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Codea" and "Codea Open Source Project" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact the project administrator.
 *
 * 5. Products derived from this software may not be called "Codea",
 *    nor may "Codea" appear in their name, without prior written
 *    permission.
 *
 * 6. All conditions set forth in the Apache Software Foundation
 *    license must be met in addition to those set forth in this
 *    license agreement.  For details regarding the Apache Software
 *    Foundation license, please see http://www.apache.org/LICENSE.txt.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 */

#ifndef __CODEA_EXCEPTION_H__
#define __CODEA_EXCEPTION_H__

/**
 * @file codea_exception.h
 * @brief Contains base class for Codea exception handling.
 */

#include <exception>

/** Base class for Codea exception handling. */
class CodeaException : public exception
{
private:
	std::string _what; /**< Exception descriptive information. */
	apr_status_t _errno; /**< Apache APR errno value, if any. */

	long _tenpow( int power )
	{
		static long tp_table[19] = 
		{
			0L,
			10L,
			100L,
			1000L,
			10000L,
			100000L,
			1000000L,
			10000000L,
			100000000L,
			1000000000L,
			10000000000L,
			100000000000L,
			1000000000000L,
			10000000000000L,
			100000000000000L,
			1000000000000000L,
			10000000000000000L,
			100000000000000000L,
			1000000000000000000L
		};
		return tp_table[power];
	}

	void _AppendFileAndLine( const char* file, long line )
	{
		assert( line >= 0L );
		_what += " in file " + _file;
		if ( line )
		{
			_what += ", line #";

			// avoid using apr_psprintf because we don't want to have to pass a pool in to the exception.
			char digits[20]; // 19 max digits in 64 bit signed long
			long subv;
			for ( long elem = 0L; elem<20; ++elem )
			{
				if ( line ) {
					subv = line % _tenpow(elem+1);
					if ( elem )
						digits[elem] = static_cast<char>( (subv / _tenpow(elem)) + '0' );
					else
						digits[elem] = static_cast<char>( subv + '0' );
					line -= subv;
					if ( (line / 10) < _tenpow(elem+1) )
					{
						// Special case to get the last digit.
						// If line is sufficiently large, we can't get a power of 10 higher than it.
						// Still, anyone who has files with that many lines needs to be beaten...
						for ( digits[elem+1] = 0; line>0; line-=_tenpow(elem+1) );
						digits[elem+1] += '0';
						break;
					}
				}
				else
					break;
			}
			for ( --elem; elem >= 0; --elem )
				_what += digits[elem];
		}
	}
public:
	/**
	 * @brief CodeaException constructor.
	 * @param problem - String describing the nature of the exception (required).
	 * @param reason - String outlining the reason (if known) for the exception (optional).
	 */
	CodeaException( const std::string& problem, const std::string& reason=std::string(), const char* file=0, long line=0L )
		:
	_what( problem ), _errno( APR_SUCCESS )
	{
		assert( line >= 0L );
		if ( file )
			_AppendFileAndLine( file, line );
		if ( reason.length() )
			_what += " (Reason:  " + reason + ")";
	}

	/**
	 * @brief CodeaException constructor.
	 * @param problem - String describing the nature of the exception (required).
	 * @param errno - Apache APR errno value outlining the reason for the exception.
	 */
	CodeaException( const std::string& problem, apr_status_t errno, const char* file=0, long line=0L )
		:
	_what( problem ), _errno( errno )
	{
		assert( line >= 0L );
		if ( file )
			_AppendFileAndLine( file, line );
		static char errbuf[256];
		apr_strerror( errno, errbuf, 255 );
		_what += "(Reason:  " + errbuf + ")";
	}

	/** Return a description of this exception. */
	virtual const char* what() { return _what.c_str(); }

	apr_status_t GetErrno() const { return _errno; }
};

class CodeaFileException : public CodeaException
{
public:
	CodeaFileException( const std::string& problem, const std::string& reason=std::string(), const char* file=0, long line=0L )
		:
	CodeaException( problem, reason, file, line )
	{}
	CodeaFileException( const std::string& problem, apr_status_t errno, const char* file=0, long line=0L )
		:
	CodeaException( problem, errno, file, line )
	{}
};

class CodeaMutexException : public CodeaException
{
public:
	CodeaMutexException( const std::string& problem, const std::string& reason=std::string(), const char* file=0, long line=0L )
		:
	CodeaException( problem, reason, file, line )
	{}
	CodeaMutexException( const std::string& problem, apr_status_t errno, const char* file=0, long line=0L )
		:
	CodeaException( problem, errno, file, line )
	{}
};

#endif // __CODEA_EXCEPTION_H__