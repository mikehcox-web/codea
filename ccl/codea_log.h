/* ====================================================================
 * The C++ Object-oriented Development Environment for Apache (Codea)
 * Open Source Project License Version 1.0
 *
 * Copyright (c) 2001 The Codea Open Source Project.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Codea Open Source Project and is based upon software
 *        written by the Apache Software Foundation.  Codea is
 *        not a derivative work of Apache nor is Codea endorsed
 *        by the Apache Software Foundation."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Codea" and "Codea Open Source Project" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact the project administrator.
 *
 * 5. Products derived from this software may not be called "Codea",
 *    nor may "Codea" appear in their name, without prior written
 *    permission.
 *
 * 6. All conditions set forth in the Apache Software Foundation
 *    license must be met in addition to those set forth in this
 *    license agreement.  For details regarding the Apache Software
 *    Foundation license, please see http://www.apache.org/LICENSE.txt.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 */

#ifndef __CODEA_LOG_H__
#define __CODEA_LOG_H__

/**
 * @file codea_log.h
 * @brief Definition of Codea logging facilities.
 */

#include "codea_core.h"

/**
 * Log management class.
 *
 * Use this class to write messages to the Apache error log.
 */
class CodeaLog
{
public:
	/** Type to identify whether or not to display system errors */
	typedef enum
	{
		SUPPRESS_SYSTEM_ERROR,
		DISPLAY_SYSTEM_ERROR
	} syserr_flag_t;

private:
	/** Type to identify which memory source this CodeaLog instance uses for logging messages. */
	typedef enum 
	{
		POOL,    /**< Use an apr_pool_t pointer to write to the log. */
		SERVER,  /**< Use a server_rec pointer to write to the log. */
		REQUEST, /**< Use a request_rec pointer to write to the log. */
		UNKNOWN  /**< Unknown memory source. */
	} memorysource_t;

	memorysource_t mem_source; /**< Store the specified memory source type. */

	/** Store the pointer to the memory source. */
	union
	{
		apr_pool_t* m_pPool;
		server_rec* m_pSvr;
		request_rec* m_pReq;
	};

public:
	/**
	 * @brief Construct a CodeaLog using an apr_pool_t pointer as the memory source.
	 * @param ppool - pointer to the apr_pool_t.
	 */
	CodeaLog( apr_pool_t* ppool )
		:
		mem_source( POOL ),
		m_pPool( ppool )
	{}

	/**
	 * @brief Construct a CodeaLog using a server_rec pointer as the memory source.
	 * @param psvr - pointer to the server_rec.
	 */
	CodeaLog( server_rec* psvr )
		:
		mem_source( SERVER ),
		m_pSvr( psvr )
	{}

	/**
	 * @brief Construct a CodeaLog using a request_rec pointer as the memory source.
	 * @param preq - pointer to the request_rec.
	 */
	CodeaLog( request_rec* preq )
		:
		mem_source( REQUEST ),
		m_pReq( preq )
	{}

	/**
	 * @brief Log a message with severity APLOG_EMERG.
	 * @param msg - The message to log
	 * @param showSysErr - Tell whether to show system error messages (optional, default is SUPPRESS_SYSTEM_ERROR)
	 */
	void Emerg( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
	{ operator()( msg, showSysErr, APLOG_EMERG ); }
	/**
	 * @brief Log a message with severity APLOG_ALERT.
	 * @param msg - The message to log
	 * @param showSysErr - Tell whether to show system error messages (optional, default is SUPPRESS_SYSTEM_ERROR)
	 */
	void Alert( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
	{ operator()( msg, showSysErr, APLOG_ALERT ); }
	/**
	 * @brief Log a message with severity APLOG_CRIT.
	 * @param msg - The message to log
	 * @param showSysErr - Tell whether to show system error messages (optional, default is SUPPRESS_SYSTEM_ERROR)
	 */
	void Crit( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
	{ operator()( msg, showSysErr, APLOG_CRIT ); }
	/**
	 * @brief Log a message with severity APLOG_ERR.
	 * @param msg - The message to log
	 * @param showSysErr - Tell whether to show system error messages (optional, default is SUPPRESS_SYSTEM_ERROR)
	 */
	void Err( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
	{ operator()( msg, showSysErr, APLOG_ERR ); }
	/**
	 * @brief Log a message with severity APLOG_WARNING.
	 * @param msg - The message to log
	 * @param showSysErr - Tell whether to show system error messages (optional, default is SUPPRESS_SYSTEM_ERROR)
	 */
	void Warning( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
	{ operator()( msg, showSysErr, APLOG_WARNING ); }
	/**
	 * @brief Log a message with severity APLOG_NOTICE.
	 * @param msg - The message to log
	 * @param showSysErr - Tell whether to show system error messages (optional, default is SUPPRESS_SYSTEM_ERROR)
	 */
	void Notice( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
	{ operator()( msg, showSysErr, APLOG_NOTICE ); }
	/**
	 * @brief Log a message with severity APLOG_INFO.
	 * @param msg - The message to log
	 * @param showSysErr - Tell whether to show system error messages (optional, default is SUPPRESS_SYSTEM_ERROR)
	 */
	void Info( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
	{ operator()( msg, showSysErr, APLOG_INFO ); }
	/**
	 * @brief Log a message with severity APLOG_DEBUG.
	 * @param msg - The message to log
	 * @param showSysErr - Tell whether to show system error messages (optional, default is SUPPRESS_SYSTEM_ERROR)
	 */
	void Debug( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
	{ operator()( msg, showSysErr, APLOG_DEBUG ); }

	/**
	 * @brief Performs the actual logging of the message.
	 * @param msg - The message to log
	 * @param showSysErr - Tell whether to show system error messages (optional, default is SUPPRESS_SYSTEM_ERROR)
	 * @param severity - Indicate the logging severity (optional, default is APLOG_ERR)
	 *
	 * This function is invoked by Emerg(), Alert(), Crit(), Err(), Warning(), Notice(), Info(), and Debug()
	 * to log messages.  It checks the memorysource_t of the instance to select the proper ap_ logging call.
	 */
	void operator() ( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR, int severity=APLOG_ERR )
	{
		int _severity = showSysErr==SUPPRESS_SYSTEM_ERROR ? severity|APLOG_NOERRNO : severity;
		switch( mem_source )
		{
		case POOL:
		    ap_log_perror( APLOG_MARK, _severity, 0, m_pPool, "%s", msg.c_str() );
		    break;
		case SERVER:
		    ap_log_error( APLOG_MARK, _severity, 0, m_pSvr, "%s", msg.c_str() );
		    break;
		case REQUEST:
		    ap_log_rerror( APLOG_MARK, _severity, 0, m_pReq, "%s", msg.c_str() );
		    break;
		default:
			break;
		}
	}
};

#endif // __CODEA_LOG_H__
