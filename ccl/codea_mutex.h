/* ====================================================================
 * The C++ Object-oriented Development Environment for Apache (Codea)
 * Open Source Project License Version 1.0
 *
 * Copyright (c) 2001 The Codea Open Source Project.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Codea Open Source Project and is based upon software
 *        written by the Apache Software Foundation.  Codea is
 *        not a derivative work of Apache nor is Codea endorsed
 *        by the Apache Software Foundation."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Codea" and "Codea Open Source Project" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact the project administrator.
 *
 * 5. Products derived from this software may not be called "Codea",
 *    nor may "Codea" appear in their name, without prior written
 *    permission.
 *
 * 6. All conditions set forth in the Apache Software Foundation
 *    license must be met in addition to those set forth in this
 *    license agreement.  For details regarding the Apache Software
 *    Foundation license, please see http://www.apache.org/LICENSE.txt.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 */

#ifndef __CODEA_MUTEX_H__
#define __CODEA_MUTEX_H__

/**
 * @file codea_mutex.h
 * @brief Contains class definitions for performing mutual exclusion.
 *
 * The term "Mutex" is used loosely here to denote a mutual exclusion
 * facility.  The actual implementation may use a mutex, semaphore, or
 * any other well-known facility to perform mutual exclusion.  In most
 * cases, the actual method of performing mutual exclusion will be
 * handled via the Apache APR, which will select a method based on
 * the best available method and the MPM being used.
 */

/** Base class for CodeaMutex classes.  This is an abstract base class. */
class CodeaMutex
{
public:
	CodeaMutex() {}
	virtual ~CodeaMutex() {}

	virtual void Lock() =0;
	virtual void TryLock() =0;
	virtual void Unlock() =0;
};

/**
 * Permanent mutex.
 *
 * We need a special type of mutex for permanent locking.  This mutex
 * will obtain a resource (i.e. a file) lock that is managed by the
 * file system, not within process memory, so that the lock will be
 * persistent.  This is necessary when a resource needs to be obtained
 * in the post-config stage, for example.
 */
class CodeaPermanentMutex : public CodeaMutex
{
private:
	const std::string& m_strUniqueID;
	const std::string& m_strPath;
	const std::string& m_strFile;
	const apr_fileperms_t m_LockPermissions;
	const apr_int32_t m_fOpenFlags;
	mutable apr_pool_t* m_pPool;
	mutable apr_file_t* m_pLockfile;
	const int m_iLockFlags;
public:
	/**
	 * @brief Create a system-wide persistent mutual exclusion object, ready to be locked.
	 * @param uniq_id - A unique identifier string for this mutex.
	 * @param p - Pointer to a pool to use.
	 */
	CodeaPermanentMutex( const std::string& uniq_id, apr_pool_t* p )
		:	
	CodeaMutex(),
	m_strUniqueID( uniq_id ),
	m_strPath( "codea" ),
	m_strFile( "/system_mutex" );
	m_LockPermissions( 0777 ),
	m_fOpenFlags( APR_WRITE | APR_CREATE | APR_TRUNCATE ),
	m_pPool( p ),
	m_iLockFlags( APR_FLOCK_EXCLUSIVE )
	{}

	virtual ~CodeaPermanentMutex()
	{
		apr_file_close( m_pLockfile );
	}

	/** Obtain a lock on this resource.  Block until the lock is obtained. */
	void Lock()
	{
		while (true)
		{
			try {
				_lock( m_iLockFlags );
			}
			catch ( CodeaMutexException& cme )
			{
				// if we get an exception that says the file is locked we should loop around again.
				continue;
			}
			catch ( CodeaException ce )
			{
				// Any other exception should propogate up.
				throw;
			}
			// Got the lock - get out.
			break;
		}
	}

	/** Try to obtain a lock on this resource.  Throw an exception if the lock cannot be obtained. */
	void TryLock()
	{
		// Any exception we get from _lock, including one that says it is already locked, should be propogated up.
		_lock( m_iLockFlags | APR_FLOCK_NONBLOCK );
	}

	/** Unlock this resource. */
	void Unlock()
	{
		apr_file_unlock( m_pLockfile );
	}
protected:
	/**
	 * @brief Obtain a file lock.
	 * @param flags - Flags to send to the APR call to lock the file.  Indicates whether the call is blocking or not.
	 *
	 * This function does the following:
	 *   - Check for the existence of the "codea" directory under the server root.  Try to make the directory if not found.
	 *   - Open a file within the directory to serve as a lockable persistent resource.
	 *   - Lock the file.
	 *   - Write the unique identifier to the file, and close the file.  Keep the lock.
	 *
	 * Exceptions are thrown along the way if any critical step fails.
	 * 
	 * If the lock cannot be obtained, an exception is thrown.  The caller should know what to do.
	 */
	void _lock( int flags )
	{
		apr_status_t status;

		// check for the codea directory
		if (APR_SUCCESS != (status = apr_dir_make( m_strPath.c_str(), m_LockPermissions, m_pPool )))
		{
			if (! APR_STATUS_IS_EEXIST( status ))
			{
				throw CodeaFileException( "Unable to create directory " + m_strPath, status, __FILE__, __LINE__ );
			}
			//  directory exists
		} // else directory created

		const std::string& file = m_strPath + m_strFile + "." + m_strUniqueID;
		if (APR_SUCCESS != (status = apr_file_open( &m_pLockfile, file.c_str(), m_fOpenFlags, m_LockPermissions, m_pPool )))
		{
			if ( APR_STATUS_IS_EACCES( status ) || APR_STATUS_IS_EPERM( status ))
			{
				throw CodeaFileException( "Unable to open file " + file, status, __FILE__, __LINE__ );
			}
			else if ( APR_STATUS_IS_EAGAIN( status ) || APR_STATUS_IS_EBUSY( status ))
			{
				// The file is already locked.  Let the caller of this function decide what to do.
				throw CodeaMutexException( "Unable to open locked file " + file, status, __FILE__, __LINE__ );
			}
		}

		if (APR_SUCCESS != (status = apr_file_lock( m_pLockfile, flags )))
		{
			// Again, the file is already locked.  Let the caller of this function decide what to do.
			throw CodeaMutexException( "Unable to lock file " + file, status, __FILE__, __LINE__ );
		}
		// We got the lock.

		apr_file_puts( m_strUniqueID.c_str(), m_pLockfile );
		apr_file_flush( m_pLockfile );
	}
};

/**
 * Abstraction of APR mutexes.
 *
 * Scope is one of APR_CROSS_PROCESS, APR_INTRAPROCESS, or APR_LOCKALL.
 * Prefer the CodeaServerMutex class for APR_LOCKALL scope,
 * the CodeaCrossprocMutex class for APR_CROSS_PROCESS scope,
 * and the CodeaIntraprocMutex class for APR_INTRAPROCESS scope.
 */
class CodeaSystemMutex : public CodeaMutex
{
protected:
	apr_lockscope_e m_LockScope;
	apr_locktype_e m_LockType;
	apr_lock_t* m_pLock;
public:
	CodeaSystemMutex( scope, type=APR_MUTEX )
		: CodeaMutex(), m_LockScope( scope ), m_LockType( type )
	{
		apr_status_t status;
		if ( APR_SUCCESS != (status = apr_lock_create(&m_pLock, m_LockType, m_LockScope, file, pool)) )
			throw CodeaMutexException( "Unable to create mutex", status, __FILE__, __LINE__ );
	}
	virtual ~CodeaSystemMutex()
	{
		apr_lock_destroy( m_pLock );
	}

	void Lock()
	{
		apr_status_t status;
		if ( APR_SUCCESS != (status = apr_lock_acquire( m_pLock )) )
			throw CodeaMutexException( "Unable to lock mutex", status, __FILE__, __LINE__ );
	}

	void TryLock()
	{
#if 0
		// apr_lock_tryacquire is a part of the planned APR but is not implemented in 2.0.16.
		// Enable this when it becomes available.
		apr_status_t status;
		if ( APR_SUCCESS != (status = apr_lock_tryacquire( m_pLock )) )
			throw CodeaMutexException( "Unable to lock mutex", status, __FILE__, __LINE__ );
#endif
	}

	void Unlock()
	{
		apr_lock_release( m_pLock );
	}
};

/**
 * Server-wide mutex.
 *
 * This is an abstraction of the mutual exclusion primitives provided
 * by the Apache APR library.  This mutex is used during normal
 * processing to control access to a resource.  The locking will pertain
 * to other Apache processes and threads, but is not persistent like
 * CodeaPermanentMutex.
 */
class CodeaServerMutex : public CodeaSystemMutex
{
public:
	CodeaServerMutex( apr_locktype_e type=APR_MUTEX)
		: CodeaSystemMutex( APR_LOCKALL, type ) {}
};

/**
 * Cross-process mutex.
 *
 * Represents a mutex whose scope extends among processes.
 */
class CodeaCrossprocMutex : public CodeaSystemMutex
{
public:
	CodeaCrossprocMutex( apr_locktype_e type=APR_MUTEX )
		: CodeaSystemMutex( APR_CROSS_PROCESS, type ) {}
};

/**
 * Intra-process mutex.
 *
 * Represents a mutex whose scope extends within a process among threads.
 */
class CodeaIntraprocMutex : public CodeaSystemMutex
{
public:
	CodeaIntraprocMutex( apr_locktype_e type=APR_MUTEX )
		: CodeaSystemMutex( APR_INTRAPROCESS, type ) {}
};

#endif // __CODEA_MUTEX_H__ 